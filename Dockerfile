FROM ruby:2.6.5

# Create a local folder for the app assets
RUN mkdir /backend
RUN mkdir -p /backend/tmp/pids
WORKDIR /backend

# Install required tooling
RUN apt-get update && apt-get install -qq -y build-essential nodejs libpq-dev postgresql-client --fix-missing --no-install-recommends

# Set our environment variables
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

# Copy and install Gems from our Gemfile
COPY Gemfile /backend/Gemfile 
COPY Gemfile.lock /backend/Gemfile.lock

RUN gem install bundler -v 2.0.2

RUN bundle install --deployment

COPY . ./

EXPOSE 80

# Start the puma server
CMD bundle exec puma -p 80