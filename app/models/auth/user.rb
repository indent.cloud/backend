require 'bcrypt'

class Auth::User < ApplicationRecord
  belongs_to :organization

  include BCrypt

  def password
    @password ||= Password.new(password_hash)
  end

  def password=(new_password)
    @password = Password.create(new_password)
    self.password_hash = @password
  end

  def self.authenticate(identifier, password)
    user = self.find_by("username = ? OR email = ?", identifier, identifier)

    success = !user.nil? && user.password == password
    if success
      user
    else
      false
    end
  end
end
