require 'securerandom'

class Auth::Session < ApplicationRecord
  belongs_to :user

  def self.generate(user)
    token = SecureRandom.uuid
    self.create(token: token, user: user)

    token
  end
end
