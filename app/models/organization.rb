class Organization < ApplicationRecord
  belongs_to :default_issues_workflow, class_name: "Issues::Workflow", optional: true
  after_create :create_default_issues_workflow

  private
    def create_default_issues_workflow
      # Create basic states
      todo_state = Issues::State.create!(name: 'Todo', category: :todo, organization: self)
      in_progress_state = Issues::State.create!(name: 'In Progress', category: :in_progress, organization: self)
      review_state = Issues::State.create!(name: 'Under Review', category: :in_progress, organization: self)
      done_state = Issues::State.create!(name: 'Done', category: :closed, organization: self)
      rejected_state = Issues::State.create!(name: 'Rejected', category: :closed, organization: self)

      # Create the workflow
      workflow = Issues::Workflow.create!(
        name: 'Default Issues Workflow',
        states_id: [todo_state.id, in_progress_state.id, review_state.id, done_state.id, rejected_state.id],
        initial_state: todo_state,
        organization: self
      )

      # Create basic transitions
      start_work_transition = Issues::Transition.create!(
        name: 'Start Work', from_state: todo_state, to_state: in_progress_state, workflow: workflow, organization: self
      )
      rejected_todo_transition = Issues::Transition.create!(
        name: 'Todo', from_state: in_progress_state, to_state: todo_state, workflow: workflow, organization: self
      )
      in_review_transition = Issues::Transition.create!(
        name: 'For Review', from_state: in_progress_state, to_state: review_state, workflow: workflow, organization: self
      )
      rework_transition = Issues::Transition.create!(
        name: 'Re-work', from_state: review_state, to_state: in_progress_state, workflow: workflow, organization: self
      )
      done_inprogress_transition = Issues::Transition.create!(
        name: 'Complete', from_state: in_progress_state, to_state: done_state, workflow: workflow, organization: self
      )
      done_review_transition = Issues::Transition.create!(
        name: 'Complete', from_state: review_state, to_state: done_state, workflow: workflow, organization: self
      )
      reopen_done_transition = Issues::Transition.create!(
        name: 'Open', from_state: done_state, to_state: todo_state, workflow: workflow, organization: self
      )
      reopen_rejected_transition = Issues::Transition.create!(
        name: 'Open', from_state: rejected_state, to_state: todo_state, workflow: workflow, organization: self
      )

      rejected_todo_transition = Issues::Transition.create!(
        name: 'Reject', from_state: todo_state, to_state: rejected_state, workflow: workflow, organization: self
      )
      rejected_inprogress_transition = Issues::Transition.create!(
        name: 'Reject', from_state: in_progress_state, to_state: rejected_state, workflow: workflow, organization: self
      )
      rejected_review_transition = Issues::Transition.create!(
        name: 'Reject', from_state: review_state, to_state: rejected_state, workflow: workflow, organization: self
      )

      self.default_issues_workflow = workflow
      self.save!
    end
end
