class Issues::State < ApplicationRecord
  belongs_to :organization
  enum category: [:todo, :in_progress, :closed]

  def transitions
    Issues::Transition.where(from_state: self)
  end
end
