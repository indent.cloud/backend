class Issues::Transition < ApplicationRecord
  belongs_to :organization
  belongs_to :workflow

  belongs_to :from_state, class_name: "Issues::State"
  belongs_to :to_state, class_name: "Issues::State"
end
