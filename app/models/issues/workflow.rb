class Issues::Workflow < ApplicationRecord
  belongs_to :organization
  belongs_to :initial_state, class_name: "Issues::State"

  def states
    Issues::State.where(id: states_id)
  end
end
