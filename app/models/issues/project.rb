class Issues::Project < ApplicationRecord
  belongs_to :organization
  belongs_to :workflow, optional: true
  enum project_type: [:todo, :workflow]

  before_create :set_initial_workflow

  private
    def set_initial_workflow
      self.workflow = self.organization.default_issues_workflow
    end
end
