class Issues::Issue < ApplicationRecord
  include RankedModel
  ranks :ranking, :with_same => :project_id 
  belongs_to :project
  belongs_to :reporter, class_name: 'Auth::User'
  belongs_to :state, optional: true
  enum priority: [:unknown, :lowest, :low, :medium, :high, :urgent]

  validates :title, presence: true
  validates :description, presence: true

  before_create :set_initial_state

  def key
    "#{project.identifier}-#{local_id}"
  end

  def assignees
    Auth::User.where(id: assignees_id).select(:id, :username, :display_name)
  end

  def watchers
    Auth::User.where(id: watchers_id).select(:id, :username, :display_name)
  end

  def workflow
    project.workflow
  end

  def self.find_by_key(organization, key)
    #TODO: better error handling
    split_key = key.split('-')
    project = split_key[0]
    local_id = split_key[1]

    project = Issues::Project.find_by(organization: organization, identifier: project)
    issue = Issues::Issue.find_by(project: project, local_id: local_id)

    issue
  end

  private
    def set_initial_state
      self.state_id = self.project.workflow.initial_state.id
    end
end
