class DebugController < ApplicationController
  def whoami
    org = Organization.find_by(identifier: request.subdomain)
    render :json => {
      :user         => nil,
      :organization => org
    }
  end
end
