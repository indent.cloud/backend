class Organization::SettingsController < ApplicationController
  def info
    render :json => @organization, :only => [:identifier, :name, :enabled]
  end
end
