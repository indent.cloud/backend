class Issues::BaseController < ApplicationController
  def get_project
    if params[:controller] == 'issues/projects'
      project_id = params[:id]
    else
      project_id = params[:project_id]
    end

    @project = Issues::Project.find_by(organization: @organization, identifier: project_id)

    if @project == nil
      render :json => {
        :error   => 'Issues/ProjectNotFound',
        :message => "A project with the name of #{params[:id]} cannot be found"
      }, status: :not_found

      return
    end
  end
end