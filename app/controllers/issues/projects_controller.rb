class Issues::ProjectsController < Issues::BaseController
  before_action :get_project, only: [:show]

  def index
    render :json => Issues::Project.where(organization: @organization)
  end

  def show
    render :json => @project
  end
end
