class Issues::IssuesController < Issues::BaseController
  before_action :get_project, only: [:index]

  def index
    render :json => Issues::Issue.rank(:ranking).where(project_id: @project.id).as_json(
      only: [:id, :title, :created_at, :assignees, :tags, :local_id, :ranking, :state],
      methods: [:key, :assignees]
    )
  end

  def show
    render :json => Issues::Issue.find_by_key(@organization, params[:id]).as_json(issue_render)
  end

  def create
    # Create the new issue
    issue = Issues::Issue.create(issue_params)
    issue.reporter = @user

    issue.save!

    # Little hack to get the local_id for the issue after it's created as Rails doesnt know about that yet

    issue = Issues::Issue.find(issue.id)

    render :json => issue.as_json(methods: :key)
  end

  def update
    issue = Issues::Issue.find_by_key(@organization, params[:id])
    if issue.update(issue_params)
      render :json => {
        success: true,
        issue: issue.as_json(issue_render)
      }
    else
      render :json => {
        :error   => 'Issues/Issue/UpdateFailed',
        :message => 'Could not update the issue'
      }, status: :bad_request
    end
  end

  private
    def issue_params
      params.require(:issue).permit(:title, :description, :project_id, :state_id, :reporter, :priority)
    end

    def issue_render
      {
        include: [
          :reporter => {
            only: [:id, :username, :display_name]
          }, 
          :state => {
            include: [:transitions => {
              include: [:from_state, :to_state]
            }]
          }
        ],
        methods: [:key, :assignees, :workflow]
      }
    end
end
