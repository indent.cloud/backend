include ::ActionController::Cookies

class ApplicationController < ActionController::API
  before_action :get_organization, :get_user

  def health
    render :json => {:success => true}
  end

  def get_organization
    org_name = request.subdomain
    @organization = Organization.find_by(identifier: org_name)

    if @organization == nil
      render :json => {
        :error   => 'Organization/NotFound',
        :message => "An organization with the name of #{org_name} cannot be found"
      }, status: :not_found

      return
    end

    if !@organization.enabled
      render :json => {
        :error   => 'Organization/Disabled',
        :message => 'This organization is currently disabled'
      }, status: :locked

      return
    end
  end

  def get_user
    session = Auth::Session.find_by(token: cookies[:auth_token])

    if session.nil?
      render :json => {
        :error   => 'Auth/NotAuthenticated',
        :message => 'You must be logged in to access this resource'
      }, status: :unauthorized
      
      return
    end

    @user = session.user
  end
end
