class Auth::SessionController < ApplicationController
  skip_before_action :get_user, only: [:create]

  def create
    user = Auth::User.authenticate(params[:username], params[:password])
    
    if user
      token = Auth::Session.generate(user)
      cookies.permanent[:auth_token] = token
      render :json => {success: true, token: token, user: user}
    else
      render :json => {
        :error   => 'Auth/Failed',
        :message => 'The username or password supplied was incorrect'
      }, status: :unauthorized

      return
    end
  end

  def destroy
    cookies.delete :auth_token
  end

  def whoami
    render :json => @user
  end
end
