Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get '/health', to: 'application#health'
  scope :api do
    get '/debug/whoami', to: 'debug#whoami'

    namespace :organization do
      scope :v1 do
        get 'info', to: 'settings#info'
      end
    end

    namespace :auth do
      post 'login', to: 'session#create'
      delete 'logout', to: 'session#destroy'
      get 'whoami', to: 'session#whoami'
    end

    namespace :issues do
      scope :v1 do
        resources :projects do
          resources :issues, only: [:index]
        end

        resources :issues, except: [:index]
        patch 'rank', to: 'issue#rank'
      end
    end
  end
end
