require_relative '../lib/state_machine'
require 'json'

todo = StateMachine::State.new(
  :todo, 
  'Todo',
  [StateMachine::Transition.new(1, 'Start work', :in_progress)]
)
in_progress = StateMachine::State.new(
  :in_progress,
  'In Progress',
  [StateMachine::Transition.new(2, 'Code Review', :code_review), StateMachine::Transition.new(3, 'Awaiting Feedback', :feedback)]
)
code_review = StateMachine::State.new(
  :code_review,
  'Code Review',
  [StateMachine::Transition.new(4, 'Done', :done), StateMachine::Transition.new(5, 'Awaiting Feedback', :feedback), StateMachine::Transition.new(6, 'In Progress', :in_progress)]
)
awaiting_feedback = StateMachine::State.new(
  :feedback, 
  'Awaiting Feedback',
  [StateMachine::Transition.new(7, 'Done', :done), StateMachine::Transition.new(8, 'Code Review', :code_review), StateMachine::Transition.new(9, 'In Progress', :in_progress)]
)
done = StateMachine::State.new(
  :done,
  'Done',
  [StateMachine::Transition.new(10, 'Start Again', :todo)]
)
rejected = StateMachine::State.new(
  :done,
  'Rejected',
  [StateMachine::Transition.new(11, 'Start Again', :todo)]
)

state_machine = StateMachine::Machine.new(
  states = [todo, in_progress, code_review, awaiting_feedback, done, rejected],
  initial_state = :todo
)

puts state_machine.state.name

state_machine.transition(:in_progress)
puts state_machine.state.name

state_machine.transition(:code_review)
puts state_machine.state.name

state_machine.transition(:feedback)
puts state_machine.state.name

state_machine.transition(:done)
puts state_machine.state.name

# state_machine.transition(:code_review)
# puts state_machine.state.name

puts state_machine.to_json()

# puts state_machine.can_transition?(:code_review)