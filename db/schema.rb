# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_28_125202) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "auth_sessions", force: :cascade do |t|
    t.string "token", null: false
    t.integer "user_id", null: false
    t.datetime "last_used"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "auth_users", force: :cascade do |t|
    t.string "username", null: false
    t.string "email", null: false
    t.string "password_hash", null: false
    t.string "display_name", null: false
    t.integer "organization_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "issues_issues", force: :cascade do |t|
    t.integer "project_id", null: false
    t.integer "local_id", null: false
    t.string "title", null: false
    t.string "description", null: false
    t.integer "reporter_id", null: false
    t.integer "assignees_id", default: [], array: true
    t.integer "waterchers_id", default: [], array: true
    t.string "tags", default: [], array: true
    t.integer "priority", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "ranking"
    t.integer "state_id"
  end

  create_table "issues_projects", force: :cascade do |t|
    t.string "identifier", null: false
    t.integer "project_type", null: false
    t.string "name", null: false
    t.integer "organization_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "workflow_id"
  end

  create_table "issues_states", force: :cascade do |t|
    t.string "name"
    t.integer "category", default: 0
    t.integer "organization_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "issues_transitions", force: :cascade do |t|
    t.string "name"
    t.integer "from_state_id"
    t.integer "to_state_id"
    t.integer "organization_id"
    t.integer "workflow_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "issues_workflows", force: :cascade do |t|
    t.string "name"
    t.integer "organization_id"
    t.integer "states_id", default: [], array: true
    t.integer "initial_state_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "organizations", force: :cascade do |t|
    t.string "identifier", null: false
    t.string "name", null: false
    t.boolean "enabled", default: true
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "default_issues_workflow_id"
  end

end
