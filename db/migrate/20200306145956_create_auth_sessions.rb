class CreateAuthSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :auth_sessions do |t|
      t.string :token, null: false
      t.integer :user_id, null: false
      t.timestamp :last_used

      t.timestamps
    end
  end
end
