class AddDefaultIssuesWorkflowToOrganization < ActiveRecord::Migration[6.0]
  def change
    add_column :organizations, :default_issues_workflow_id, :integer, null: true
  end
end
