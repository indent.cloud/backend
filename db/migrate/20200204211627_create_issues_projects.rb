class CreateIssuesProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :issues_projects do |t|
      t.string :identifier, null: false
      t.integer :project_type, null: false
      t.string :name, null: false
      t.integer :organization_id, null: false

      t.timestamps
    end

    # Create a sequence for every issues project so we can generate the local issue id
    #   so we can have issues keys like INDENT-1, INDENT-2 that are linear for the project
    execute <<-SQL
      CREATE FUNCTION create_issues_project_local_id_sequence() RETURNS trigger
          LANGUAGE plpgsql
          AS $$
      begin
        execute format('create sequence issues_project_local_id_sequence_%s', NEW.id);
        return NEW;
      end
      $$;

      CREATE TRIGGER create_issues_project_local_id_sequence AFTER INSERT ON issues_projects FOR EACH ROW EXECUTE PROCEDURE create_issues_project_local_id_sequence();
    SQL
  end
end
