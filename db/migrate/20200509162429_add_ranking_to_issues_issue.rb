class AddRankingToIssuesIssue < ActiveRecord::Migration[6.0]
  def change
    add_column :issues_issues, :ranking, :integer

    Issues::Issue.update_all('ranking = EXTRACT(EPOCH FROM created_at)')
  end
end
