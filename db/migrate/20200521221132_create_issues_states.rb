class CreateIssuesStates < ActiveRecord::Migration[6.0]
  def change
    create_table :issues_states do |t|
      t.string :name
      t.integer :category, default: 0
      t.integer :organization_id

      t.timestamps
    end
  end
end
