class CreateAuthUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :auth_users do |t|
      t.string :username, null: false
      t.string :email, null: false
      t.string :password_hash, null: false
      t.string :display_name, null: false
      t.integer :organization_id, null: false

      t.timestamps
    end
  end
end
