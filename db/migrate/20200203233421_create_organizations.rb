class CreateOrganizations < ActiveRecord::Migration[6.0]
  def change
    create_table :organizations do |t|
      t.string :identifier, null: false
      t.string :name, null: false
      t.boolean :enabled, default: true

      t.timestamps
    end
  end
end
