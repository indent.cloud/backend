class CreateIssuesTransitions < ActiveRecord::Migration[6.0]
  def change
    create_table :issues_transitions do |t|
      t.string :name
      t.integer :from_state_id
      t.integer :to_state_id
      t.integer :organization_id
      t.integer :workflow_id

      t.timestamps
    end
  end
end
