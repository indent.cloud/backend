class AddWorkflowToIssuesProject < ActiveRecord::Migration[6.0]
  def change
    add_column :issues_projects, :workflow_id, :integer
  end
end
