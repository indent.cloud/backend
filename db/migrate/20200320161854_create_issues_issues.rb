class CreateIssuesIssues < ActiveRecord::Migration[6.0]
  def change
    create_table :issues_issues do |t|
      t.integer :project_id, null: false
      t.integer :local_id, null: false
      t.string :title, null: false
      t.string :description, null: false
      t.integer :reporter_id, null: false
      t.integer :assignees_id, array: true, default: []
      t.integer :waterchers_id, array: true, default: []
      t.string :tags, array: true, default: []
      t.integer :priority, default: 0

      t.timestamps
    end

    # Use the project sequence to generate the local id
    #   so we can have issues keys like INDENT-1, INDENT-2 that are linear for the project
    execute <<-SQL
      CREATE FUNCTION fill_in_issues_project_local_id_sequence() RETURNS trigger
          LANGUAGE plpgsql
          AS $$
      begin
        NEW.local_id := nextval('issues_project_local_id_sequence_' || NEW.project_id);
        RETURN NEW;
      end
      $$;

      CREATE TRIGGER fill_in_issues_project_local_id_sequence BEFORE INSERT ON issues_issues FOR EACH ROW EXECUTE PROCEDURE fill_in_issues_project_local_id_sequence();
    SQL
  end
end
