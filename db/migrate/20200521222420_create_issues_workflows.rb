class CreateIssuesWorkflows < ActiveRecord::Migration[6.0]
  def change
    create_table :issues_workflows do |t|
      t.string :name
      t.integer :organization_id
      t.integer :states_id, array: true, default: []
      t.integer :initial_state_id

      t.timestamps
    end
  end
end
