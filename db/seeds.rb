# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# Create the test organization
org_test = Organization.create!(name: 'Indent Test', identifier: 'test', enabled: true)

user_test = Auth::User.create!(username: 'test', email: 'test@indent.cloud', display_name: 'Test McTest', password: 'test123!', organization: org_test)
user_dave = Auth::User.create!(username: 'dave.smith', email: 'dave.smith@indent.cloud', display_name: 'Dave Smith', password: 'D@v1dSm1ith', organization: org_test)

issues_project_indent = Issues::Project.create!(identifier: 'INDENT', project_type: :workflow, name: 'Indent Engineering', organization: org_test)
issues_project_infra = Issues::Project.create!(identifier: 'INFRA', project_type: :todo, name: 'Infrastructure', organization: org_test)
issues_project_android = Issues::Project.create!(identifier: 'ANDROID', project_type: :workflow, name: 'Android App', organization: org_test)
issues_project_ios = Issues::Project.create!(identifier: 'IOS', project_type: :workflow, name: 'iOS App', organization: org_test)
issues_project_marketing = Issues::Project.create!(identifier: 'MARKETING', project_type: :todo, name: 'Marketing', organization: org_test)
issues_project_finance = Issues::Project.create!(identifier: 'FIN', project_type: :workflow, name: 'Financing', organization: org_test)


Issues::Issue.create!(title: 'A test issue', description: 'This should be a long test but whatever', reporter: user_test, assignees_id: [user_test.id, user_dave.id], tags: ['Test', 'Another'], project: issues_project_indent)
Issues::Issue.create!(title: 'Ability to create issues', description: 'A user should obviously be able to create issues', reporter: user_test, tags: ['API', 'WebUI'], project: issues_project_indent)
Issues::Issue.create!(title: 'Make it work', description: 'Everything should work', reporter: user_test, tags: ['Important'], project: issues_project_infra)
Issues::Issue.create!(title: 'Ability to view issues', description: 'A user should obviously be able to view the issues that they and others have created', reporter: user_test, assignees_id: [user_test.id], tags: ['API', 'WebUI'], project: issues_project_indent)